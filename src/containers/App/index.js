import React, { Component } from "react";
import Gallery from "../Gallery";
import res from "../../images.json";

class App extends Component {
  constructor() {
    super();
    this.state = {
      timer: 0
    };
  }
  render() {
    return (
      <div className="App">
        <Gallery images={res.images} timer={this.state.timer} slides={1} />
        <hr />
        <label htmlFor="timer">Timer:</label><br />
        <select
          name="timer"
          id="timer"
          defaultValue={this.state.timer.toString()}
          onChange={e => {
            this.setState({ timer: parseInt(e.target.value, 10) });
          }}
        >
          <option value="0">Stop Autoplay</option>
          <option value="2000">2 Seconds</option>
          <option value="3000">3 Seconds</option>
          <option value="4000">4 Seconds</option>
          <option value="5000">5 Seconds</option>
        </select>
      </div>
    );
  }
}

export default App;
