import React, { Component } from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";
import ReactInterval from "react-interval";
import Slide from "../../components/Slide";
import "./styles.scss";

class Gallery extends Component {
  constructor() {
    super();
    this.state = {
      currentSlide: 0,
      isPaused: false,
      slideSize: 0
    };
    this.isResizing = debounce(this.isResizing, 500);
  }
  componentDidMount() {
    this.updateSlideWidth();
    window.addEventListener("resize", () => {
      this.updateSlideWidth(true);
    });
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateSlideWidth);
  }
  updateSlideWidth = (isResizing = false) => {
    this.setState(
      {
        slideSize: this.slider.clientWidth,
        resizing: isResizing
      },
      () => {
        this.updateSlide(this.state.currentSlide);
        this.isResizing();
      }
    );
  };
  isResizing = () => {
    this.setState({
      resizing: false
    });
  };
  updateSlide = currentSlide => {
    this.sliderWrapper.style.left = `-${currentSlide * this.state.slideSize}px`;
  };
  prevSlide = () => {
    const images_length = this.props.images.length - 1;
    this.setState(
      {
        currentSlide: this.state.currentSlide === 0
          ? images_length
          : this.state.currentSlide - 1
      },
      () => {
        this.updateSlide(this.state.currentSlide);
      }
    );
  };
  nextSlide = () => {
    const images_length = this.props.images.length / this.props.slides - 1;
    this.setState(
      {
        currentSlide: this.state.currentSlide >= images_length
          ? 0
          : this.state.currentSlide + 1
      },
      () => {
        this.updateSlide(this.state.currentSlide);
      }
    );
  };
  updatePaused = isPaused => {
    if (this.state.isPaused !== isPaused) {
      this.setState({
        isPaused
      });
    }
  };
  render() {
    return (
      <div
        className="gallery__container"
        onMouseEnter={() => this.updatePaused(true)}
        onMouseLeave={() => this.updatePaused(false)}
      >
        {!!this.props.timer === true &&
          <ReactInterval
            enabled={!this.state.isPaused}
            timeout={this.props.timer}
            callback={this.nextSlide}
          />}
        <div
          className="gallery"
          ref={element => {
            this.slider = element;
          }}
        >
          <div
            className={`gallery__wrapper${this.state.resizing
              ? " resizing"
              : ""}`}
            ref={element => {
              this.sliderWrapper = element;
            }}
            style={{
              width: this.state.slideSize * this.props.images.length
            }}
          >
            {this.props.images.map((image, idx) =>
              <Slide
                src={image}
                width={this.state.slideSize / this.props.slides}
                key={idx}
              />
            )}
          </div>
        </div>
        {!!this.props.timer === true &&
          this.state.isPaused &&
          <div className="gallery__paused" />}
        <a href="#" onClick={this.prevSlide} className="gallery__prev" />
        <a href="#" onClick={this.nextSlide} className="gallery__next" />
      </div>
    );
  }
}

Gallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
  slides: PropTypes.number
};

Gallery.defaultProps = {
  slides: 1,
  timer: 3000
};

export default Gallery;
