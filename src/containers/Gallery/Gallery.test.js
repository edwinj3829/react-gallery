import React from "react";
import { expect } from "chai";
import { mount, shallow } from "enzyme";
import Gallery from "./";
import Slide from "../../components/Slide";
import res from "../../images.json";

const ShallowGallery = props => {
  return shallow(<Gallery {...props} />);
};
const MountGallery = props => {
  return mount(<Gallery {...props} />);
};

describe("containers/Gallery", () => {
  it("renders 6 Slide components", () => {
    const wrapper = ShallowGallery({
      images: res.images,
      timer: 0
    });
    expect(wrapper.find(Slide).length).to.equal(6);
  });
  describe("when paused", () => {
    it("shows icon if timer is set", () => {
      const wrapper = ShallowGallery({
        images: res.images,
        timer: 3000
      });
      wrapper.setState({
        isPaused: true
      });
      expect(wrapper.find(".gallery__paused").length).to.equal(1);
    });
    it("hides icon if timer is not set", () => {
      const wrapper = ShallowGallery({
        images: res.images,
        timer: 0
      });
      wrapper.setState({
        isPaused: true
      });
      expect(wrapper.find(".gallery__paused").length).to.equal(0);
    });
  });
  describe("when clicking the next slide button", () => {
    it("displays the next slide", () => {
      const wrapper = MountGallery({
        images: res.images,
        timer: 0
      });
      wrapper.find(".gallery__next").simulate("click");
      expect(wrapper.state().currentSlide).to.equal(1);
    });
  });

  describe("when clicking the prev slide button", () => {
    it("displays the prev slide", () => {
      const wrapper = MountGallery({
        images: res.images,
        timer: 0
      });
      wrapper.find(".gallery__prev").simulate("click");
      expect(wrapper.state().currentSlide).to.equal(res.images.length - 1);
    });
  });
});
