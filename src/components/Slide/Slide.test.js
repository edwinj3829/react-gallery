import React from "react";
import { expect } from "chai";
import { mount, shallow } from "enzyme";
import Slide from "./";
import res from "../../images.json";

const ShallowSlide = props => {
  return shallow(<Slide {...props} />);
};

describe("components/Slide", () => {
  it("renders an image tag", () => {
    const wrapper = ShallowSlide({
      src: res.images[0]
    });
    expect(wrapper.find("img").length).to.equal(1);
  });
});
