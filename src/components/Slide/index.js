import React from "react";
import PropTypes from "prop-types";

function Slide(props) {
  return (
    <div className="slide">
      <img
        className="slide__image"
        style={{ width: props.width }}
        src={props.src}
        alt="Slide"
      />
    </div>
  );
}

Slide.propTypes = {
  src: PropTypes.string.isRequired
};

export default Slide;
